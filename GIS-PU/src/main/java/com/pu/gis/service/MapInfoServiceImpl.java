package com.pu.gis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pu.gis.dao.db.service.MapService;
import com.pu.gis.model.MapInfoModel;

@Service
@Transactional //describes transaction attributes on a method or class.
public class MapInfoServiceImpl implements MapInfoService {

	@Autowired
    private MapService mapInfoDao;

	@Override
	public void addMapInfo(MapInfoModel team) {
		mapInfoDao.addMapInfoModel(team);
	}

	@Override
	public void updateMapInfo(MapInfoModel team) {
		mapInfoDao.updateMapInfoModel(team);
	}

	@Override
	public MapInfoModel getMapInfo(Long uid) {
		return mapInfoDao.getMapInfoModel(uid);
	}

	@Override
	public void deleteMapInfo(Long uid) {
		mapInfoDao.deleteMapInfoModel(uid);
	}

	@Override
	public List<MapInfoModel> getMapInfos() {
		return mapInfoDao.getMapInfoModels();
	}
	
}
