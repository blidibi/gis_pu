package com.pu.gis.service;

import java.util.List;

import com.pu.gis.model.MapInfoModel;

public interface MapInfoService {
	public void addMapInfo(MapInfoModel team);

	public void updateMapInfo(MapInfoModel team);

	public MapInfoModel getMapInfo(Long uid);

	public void deleteMapInfo(Long uid);

	public List<MapInfoModel> getMapInfos();
}
