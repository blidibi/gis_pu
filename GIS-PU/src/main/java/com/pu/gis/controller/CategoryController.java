package com.pu.gis.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pu.gis.dao.db.service.MapService;
import com.pu.gis.model.CategoryModel;

@Controller
public class CategoryController {

	@Autowired
	private MapService mapService;
	
	@RequestMapping(value = "/cat/add", method = RequestMethod.POST)
	public @ResponseBody String addCatProcess(HttpServletRequest request) {
		CategoryModel cat = new CategoryModel();
		cat.setCategory(request.getParameter("category"));
		if (mapService.saveCategory(cat)) return "OK";
		else return "FAILED";
	}
	
	@RequestMapping(value="/cat/list")
	public ModelAndView listOfCat() {
		List<CategoryModel> list=new ArrayList<CategoryModel>();
		list=mapService.getAllCategory();
		ModelAndView modelAndView = new ModelAndView("formCategory");
		modelAndView.addObject("categoryModel",list);
		return modelAndView;
	}
	
	@RequestMapping(value="/cat/edit", method=RequestMethod.POST)
	public @ResponseBody String editCatProcess(HttpServletRequest request) {
		CategoryModel updCat = new CategoryModel();
		updCat.setId(Long.valueOf(request.getParameter("uid_category")));
		updCat.setCategory(request.getParameter("category"));
		
		if (mapService.saveCategory(updCat)) return "OK";
		else return "FAILED";
	}
	
	@RequestMapping(value="/cat/delete", method=RequestMethod.POST)
	public @ResponseBody String deleteCategory(HttpServletRequest request) {
		if (mapService.deleteCategory(Long.valueOf(request.getParameter("uid_category")))) return "OK";
		else return "FAILED";
	}
	
}
