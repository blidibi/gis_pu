package com.pu.gis.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.pu.gis.dao.db.service.MapService;
import com.pu.gis.model.AreaModel;
import com.pu.gis.model.CategoryModel;
import com.pu.gis.model.MapImageModel;
import com.pu.gis.model.MapInfoModel;
import com.pu.gis.model.UploadedFile;
import com.pu.gis.model.YearModel;

@Controller
public class MapInfoController implements ServletContextAware {

	@Autowired
	private MapService mapService;



	private ServletContext servletContext;

	@RequestMapping(value = "/map/add", method = RequestMethod.GET)
	public ModelAndView addMapPage() {
		ModelAndView modelAndView = new ModelAndView("formSaveMap");
		List<AreaModel> listArea=new ArrayList<AreaModel>();
		List<CategoryModel> listCategory=new ArrayList<CategoryModel>();
		List<YearModel> listYear=new ArrayList<YearModel>();

		listArea=mapService.getAllArea();
		listCategory=mapService.getAllCategory();
		listYear=mapService.getAllYearOrderBy("year");

		modelAndView.addObject("listArea", listArea);
		modelAndView.addObject("listCategory", listCategory);
		modelAndView.addObject("listYear", listYear);

		return modelAndView;
	}

	@RequestMapping(value = "/map/add/process", method = RequestMethod.POST)
	public ModelAndView addMapProcess(HttpServletRequest request, @ModelAttribute UploadedFile uploadedFile) {
		System.out.println("latitude " + request.getParameter("latitude") + " " + request.getParameter("longitude"));
		MapInfoModel mapInfoModel = new MapInfoModel();
		mapInfoModel.setDescription(request.getParameter("description"));
		mapInfoModel.setLatitude(request.getParameter("latitude"));
		mapInfoModel.setLongitude(request.getParameter("longitude"));
		mapInfoModel.setTitle(request.getParameter("title"));
		mapInfoModel.setArea(mapService.getAreaById(Long.valueOf(request.getParameter("area"))));
		mapInfoModel.setCategory(mapService.getCategoryById(Long.valueOf(request.getParameter("category"))));
		mapInfoModel.setYear(mapService.getYearById(Long.valueOf(request.getParameter("year"))));
		mapInfoModel.setDate(request.getParameter("datehappend"));
		
		Set<MapImageModel> imageModels = new HashSet<MapImageModel>();

		InputStream inputStream = null;
		OutputStream outputStream = null;

		List<MultipartFile> files = uploadedFile.getFiles();

		if (files != null) {
			for (MultipartFile multipartFile : files) {
				if (!multipartFile.isEmpty()) {

					try {
						inputStream = multipartFile.getInputStream();

						String fileName = UUID.randomUUID().toString()+"-"+multipartFile.getOriginalFilename();
						String pathFile = servletContext.getRealPath("/") + "/resources/images/";
						System.out.println(pathFile);
						File dir = new File(pathFile);
						File newFile = new File(pathFile+fileName);
						
						if(!dir.exists()){
							dir.mkdirs();
						}
						
						if (!newFile.exists()) {
							newFile.createNewFile();
						}

						outputStream = new FileOutputStream(newFile);
						int read = 0;
						byte[] bytes = new byte[1024];

						while ((read = inputStream.read(bytes)) != -1) {
							outputStream.write(bytes, 0, read);
						}

						MapImageModel imageModel = new MapImageModel();
						imageModel.setMapInfo(mapInfoModel);
						imageModel.setImageName(fileName);

						imageModels.add(imageModel);
					} catch (IOException e) {
						e.printStackTrace();
						if (outputStream != null) {
							try {outputStream.close();} catch (IOException e1) {e1.printStackTrace();}
						}
					} finally {
						if (outputStream != null) {
							try {outputStream.close();} catch (IOException e2) {e2.printStackTrace();}
						}
					}				
				}
			}
			mapInfoModel.setImages(imageModels);
		}

		mapService.addMapInfoModel(mapInfoModel);
		String message = "Map Info was successfully added.";
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("message", message);
		return modelAndView;
	}

	@RequestMapping(value="/map/list", method = RequestMethod.GET)
	public ModelAndView listOfMaps() {
		ModelAndView modelAndView = new ModelAndView("listOfMaps");
		List<AreaModel> listArea=new ArrayList<AreaModel>();
		List<CategoryModel> listCategory=new ArrayList<CategoryModel>();
		List<YearModel> listYear=new ArrayList<YearModel>();

		listArea=mapService.getAllArea();
		listCategory=mapService.getAllCategory();
		listYear=mapService.getAllYearOrderBy("year");

		modelAndView.addObject("listArea", listArea);
		modelAndView.addObject("listCategory", listCategory);
		modelAndView.addObject("listYear", listYear);
		return modelAndView;
	}
	
	@RequestMapping(value="/map/search", method = RequestMethod.POST)
	public ModelAndView searchMapsData(HttpServletRequest request) {
		Long idArea = request.getParameter("area") != null ? Long.valueOf(request.getParameter("area")) : 0;
		Long idCategory = request.getParameter("category") != null ? Long.valueOf(request.getParameter("category")) : 0;
		Long idYear = request.getParameter("year") != null ? Long.valueOf(request.getParameter("year")) : 0;
		List<MapInfoModel> lists = mapService.getMapInfoFilter(idArea, idCategory, idYear);
		
		ModelAndView view = new ModelAndView("tableMaps");
		view.addObject("maps", lists);
		
		return view;
	}

	@RequestMapping(value="/map/edit/{uid}", method=RequestMethod.GET)
	public ModelAndView editMapPage(@PathVariable Long uid) {
		ModelAndView modelAndView = new ModelAndView("edit-map-form");
		MapInfoModel map = mapService.getMapInfoModel(uid);
		modelAndView.addObject("map",map);
		return modelAndView;
	}

	@RequestMapping(value="/map/edit/{uid}", method=RequestMethod.POST)
	public ModelAndView editMapProcess(@ModelAttribute MapInfoModel map, @PathVariable Long uid) {
		ModelAndView modelAndView = new ModelAndView("home");
		//		mapInfoService.updateMapInfo(map);
		//		mapService.
		//		String message = "Map was successfully edited.";
		//		modelAndView.addObject("message", message);
		return modelAndView;
	}

	@RequestMapping(value="/map/delete/{uid}", method=RequestMethod.GET)
	public ModelAndView deleteMap(@PathVariable Long uid) {
		ModelAndView modelAndView = new ModelAndView("home");
		//		mapInfoService.deleteMapInfo(uid);
		String message = "Map was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

	@Override
	public void setServletContext(ServletContext paramServletContext) {
		this.servletContext = paramServletContext;
	}
}
