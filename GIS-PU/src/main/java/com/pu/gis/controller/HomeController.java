package com.pu.gis.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.pu.gis.controller.modelhelper.ListMap;
import com.pu.gis.dao.db.base.MapImageDao;
import com.pu.gis.dao.db.service.MapService;
import com.pu.gis.model.AreaModel;
import com.pu.gis.model.CategoryModel;
import com.pu.gis.model.MapImageModel;
import com.pu.gis.model.MapInfoModel;
import com.pu.gis.model.YearModel;

@Controller
@RequestMapping("/gis")
public class HomeController {

	@Autowired MapService mapService;
	@RequestMapping(value = "/home")
	public ModelAndView indexPage() {
		ModelAndView modelAndView = new ModelAndView("home/home");
//		ModelAndView modelAndView = new ModelAndView("add-map-form");
		List<AreaModel> listArea=new ArrayList<AreaModel>();
		List<CategoryModel> listCategory=new ArrayList<CategoryModel>();
		List<YearModel> listYear=new ArrayList<YearModel>();
		listArea=mapService.getAllArea();
		listCategory=mapService.getAllCategory();
		listYear=mapService.getAllYear();
		modelAndView.addObject("listArea", listArea);
		modelAndView.addObject("listCategory", listCategory);
		modelAndView.addObject("listYear", listYear);
		modelAndView.addObject("map", new MapInfoModel());
		return modelAndView;
	}
	
	@RequestMapping(value = "/getdata", method = RequestMethod.POST)
	public @ResponseBody String getData(HttpServletRequest request) {
		List<ListMap> list=new ArrayList<ListMap>();
		try {
			Long area=Long.valueOf(request.getParameter("area"));
			Long year=Long.valueOf(request.getParameter("year"));
			Long category=Long.valueOf(request.getParameter("category"));
			List<MapInfoModel> l=new ArrayList<MapInfoModel>();
			l=mapService.getMapInfoFilter(area, category, year);
			for(MapInfoModel m:l){
				ListMap lm=new ListMap();
				lm.setDate(m.getDate());
				lm.setId(m.getUid());
				lm.setLatitude(m.getLatitude());
				lm.setLongitude(m.getLongitude());
				lm.setTitle(m.getTitle());
				list.add(lm);
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return new Gson().toJson(list);
	}
	
	@RequestMapping(value = "/detail_location/{uid}", method = RequestMethod.GET)
	public ModelAndView detailPage(@PathVariable String uid,HttpServletRequest request) {
		MapInfoModel model=new MapInfoModel();
		MapImageModel image = new MapImageModel();
		model=mapService.getMapInfoModel(Long.valueOf(uid));
		image = mapService.getImageByMapInfoId(Long.valueOf(uid));
		ModelAndView modelAndView = new ModelAndView("home/detailLocation");
		System.out.println("Gambar -> "+image.getImageName());
		modelAndView.addObject("mapModel",model);
		modelAndView.addObject("img",image);
		return modelAndView;
	}
}
