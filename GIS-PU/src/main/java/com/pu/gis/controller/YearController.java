package com.pu.gis.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pu.gis.dao.db.service.MapService;
import com.pu.gis.model.YearModel;

@Controller
public class YearController {

	@Autowired
	private MapService mapService;
	
	@RequestMapping(value = "/year/add", method = RequestMethod.POST)
	public @ResponseBody String addYearProcess(HttpServletRequest request) {
		YearModel year = new YearModel();
		year.setYear(Integer.valueOf(request.getParameter("year")));
		if (mapService.saveYear(year)) return "OK";
		else return "FAILED";
	}
	
	@RequestMapping(value="/year/list")
	public ModelAndView listOfYear() {
		List<YearModel> list=new ArrayList<YearModel>();
		list=mapService.getAllYear();
		ModelAndView modelAndView = new ModelAndView("formYear");
		modelAndView.addObject("yearModel",list);
		return modelAndView;
	}
	
	@RequestMapping(value="/year/edit", method=RequestMethod.POST)
	public @ResponseBody String editYearProcess(HttpServletRequest request) {
		YearModel updYear = new YearModel();
		updYear.setId(Long.valueOf(request.getParameter("uid_year")));
		updYear.setYear(Integer.valueOf(request.getParameter("year")));
		
		if (mapService.saveYear(updYear)) return "OK";
		else return "FAILED";
	}
	
	@RequestMapping(value="/year/delete", method=RequestMethod.POST)
	public @ResponseBody String deleteYear(HttpServletRequest request) {
		if (mapService.deleteYear(Long.valueOf(request.getParameter("uid_year")))) return "OK";
		else return "FAILED";
	}
	
}
