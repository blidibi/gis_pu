package com.pu.gis.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pu.gis.dao.db.service.MapService;
import com.pu.gis.model.AreaModel;

@Controller
public class AreaController {

	@Autowired
	private MapService mapService;
	
	@RequestMapping(value = "/area/add", method = RequestMethod.POST)
	public @ResponseBody String addAreaProcess(HttpServletRequest request) {
		AreaModel area = new AreaModel();
		area.setArea(request.getParameter("area"));
		if (mapService.saveArea(area)) return "OK";
		else return "FAILED";
	}
	
	@RequestMapping(value="/area/list")
	public ModelAndView listOfArea() {
		List<AreaModel> list=new ArrayList<AreaModel>();
		list=mapService.getAllArea();
		ModelAndView modelAndView = new ModelAndView("formArea");
		modelAndView.addObject("areaModel",list);
		return modelAndView;
	}
	
	@RequestMapping(value="/area/edit", method=RequestMethod.POST)
	public @ResponseBody String editAreaProcess(HttpServletRequest request) {
		AreaModel updArea = new AreaModel();
		updArea.setId(Long.valueOf(request.getParameter("uid_area")));
		updArea.setArea(request.getParameter("area"));
		
		if (mapService.saveArea(updArea)) return "OK";
		else return "FAILED";
	}
	
	@RequestMapping(value="/area/delete", method=RequestMethod.POST)
	public @ResponseBody String deleteArea(HttpServletRequest request) {
		if (mapService.deleteArea(Long.valueOf(request.getParameter("uid_area")))) return "OK";
		else return "FAILED";
	}
	
	
}
