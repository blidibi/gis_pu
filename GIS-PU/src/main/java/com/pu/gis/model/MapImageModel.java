package com.pu.gis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tb_map_image")
public class MapImageModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long uid;

	@Column(name = "image_name")
	private String imageName;

	@ManyToOne
	@JoinColumn(name = "uid_map_info")
	private MapInfoModel mapInfo;

	public MapImageModel() {
	}

	public MapImageModel(Long uid, String imageLocation, MapInfoModel mapInfo) {
		super();
		this.uid = uid;
		this.imageName = imageLocation;
		this.mapInfo = mapInfo;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public MapInfoModel getMapInfo() {
		return mapInfo;
	}

	public void setMapInfo(MapInfoModel mapInfo) {
		this.mapInfo = mapInfo;
	}

}
