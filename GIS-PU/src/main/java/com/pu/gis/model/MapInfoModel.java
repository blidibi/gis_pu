package com.pu.gis.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tb_map_info")
public class MapInfoModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long uid;

	private String title;
	private String description;
	private String date;
	private String longitude;
	private String latitude;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "mapInfo")
	private Set<MapImageModel> images;

	@ManyToOne
	@JoinColumn(name = "uid_area")
	private AreaModel area;

	@ManyToOne
	@JoinColumn(name = "uid_years")
	private YearModel year;

	@ManyToOne
	@JoinColumn(name = "uid_category")
	private CategoryModel category;

	public MapInfoModel() {
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public Set<MapImageModel> getImages() {
		return images;
	}

	public void setImages(Set<MapImageModel> imageModels) {
		this.images = imageModels;
	}

	public AreaModel getArea() {
		return area;
	}

	public void setArea(AreaModel area) {
		this.area = area;
	}

	public YearModel getYear() {
		return year;
	}

	public void setYear(YearModel year) {
		this.year = year;
	}

	public CategoryModel getCategory() {
		return category;
	}

	public void setCategory(CategoryModel category) {
		this.category = category;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	

}
