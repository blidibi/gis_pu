package com.pu.gis.dao.db.base;

import java.util.List;

import org.springframework.stereotype.Component;

import com.pu.gis.dao.db.generic.GenerikDao;
import com.pu.gis.model.YearModel;

@Component
public class YearDao extends GenerikDao<YearModel>{

	@SuppressWarnings("unchecked")
	public List<YearModel> getAllOrderBy() {
		return sessionFactory.getCurrentSession().createQuery("FROM YearModel ORDER BY year").list();
	}
}
