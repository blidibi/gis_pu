package com.pu.gis.dao.db.base;

import org.springframework.stereotype.Component;

import com.pu.gis.dao.db.generic.GenerikDao;
import com.pu.gis.model.AreaModel;

@Component
public class AreaDao extends GenerikDao<AreaModel>{

}
