package com.pu.gis.dao.db.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.pu.gis.dao.db.base.AreaDao;
import com.pu.gis.dao.db.base.CategoryDao;
import com.pu.gis.dao.db.base.MapImageDao;
import com.pu.gis.dao.db.base.MapInfoDao;
import com.pu.gis.dao.db.base.YearDao;
import com.pu.gis.dao.db.service.MapService;
import com.pu.gis.model.AreaModel;
import com.pu.gis.model.CategoryModel;
import com.pu.gis.model.MapImageModel;
import com.pu.gis.model.MapInfoModel;
import com.pu.gis.model.YearModel;

@Repository
public class MapImplement implements MapService {

	@Autowired MapInfoDao mapInfoDao;
	@Autowired MapImageDao mapImageDao;
	@Autowired AreaDao areaDao;
	@Autowired CategoryDao categoryDao;
	@Autowired YearDao yearDao;

	@Override
	@Transactional
	public void addMapInfoModel(MapInfoModel team) {
		mapInfoDao.save(team);
	}

	@Override
	@Transactional
	public void updateMapInfoModel(MapInfoModel mapInfoModel) {
		MapInfoModel updateModel = getMapInfoModel(mapInfoModel.getUid());
		updateModel.setTitle(mapInfoModel.getTitle());
		updateModel.setLongitude(mapInfoModel.getLongitude());
		updateModel.setLatitude(mapInfoModel.getLatitude());
		updateModel.setDescription(mapInfoModel.getDescription());
		updateModel.setImages(mapInfoModel.getImages());
		mapInfoDao.save(mapInfoModel);
	}

	@Override
	public MapInfoModel getMapInfoModel(Long id) {
		return mapInfoDao.getById(id);
	}

	@Override
	@Transactional
	public void deleteMapInfoModel(Long id) {
		mapInfoDao.delete(mapInfoDao.getById(id));
	}

	@Override
	public List<MapInfoModel> getMapInfoModels() {
		return mapInfoDao.getAll();
	}

	@Override
	public boolean saveCategory(CategoryModel categoryModel) {
		try {
			categoryDao.save(categoryModel);
			return true;			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean saveArea(AreaModel areaModel) {
		try {
			areaDao.save(areaModel);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean saveYear(YearModel yearModel) {
		try {
			yearDao.save(yearModel);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean deleteCategory(Long uid) {
		try {
			categoryDao.delete(categoryDao.getById(uid));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean deleteArea(Long uid) {
		try {
			areaDao.delete(areaDao.getById(uid));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean deleteYear(Long uid) {
		try {
			yearDao.delete(yearDao.getById(uid));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<AreaModel> getAllArea() {
		return areaDao.getAll();
	}

	@Override
	public List<CategoryModel> getAllCategory() {
		return categoryDao.getAll();
	}

	@Override
	public List<YearModel> getAllYear() {
		return yearDao.getAll();
	}

	@Override
	public AreaModel getAreaById(Long id) {
		return areaDao.getById(id);
	}

	@Override
	public CategoryModel getCategoryById(Long id) {
		return categoryDao.getById(id);
	}

	@Override
	public YearModel getYearById(Long id) {
		return yearDao.getById(id);
	}

	@Override
	public List<YearModel> getAllYearOrderBy(String param) {
		return yearDao.getAllOrderBy();
	}

	@Override
	public List<MapInfoModel> getMapInfoFilter(Long idArea, Long idCategory,Long idYear) {
		return mapInfoDao.getMapFilter(idArea, idCategory, idYear);
	}

	@Override
	public MapImageModel getImageByMapInfoId(Long id) {
		return mapImageDao.getMapImageModel(id);
	}

}