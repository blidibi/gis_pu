package com.pu.gis.dao.db.service;

import java.util.List;

import com.pu.gis.model.AreaModel;
import com.pu.gis.model.CategoryModel;
import com.pu.gis.model.MapImageModel;
import com.pu.gis.model.MapInfoModel;
import com.pu.gis.model.YearModel;

public interface MapService {
	public void addMapInfoModel(MapInfoModel team);
	public void updateMapInfoModel(MapInfoModel team);
	public MapInfoModel getMapInfoModel(Long id);
	public void deleteMapInfoModel(Long id);
	public List<MapInfoModel> getMapInfoModels();
	public List<MapInfoModel> getMapInfoFilter(Long idArea,Long idCategory,Long idYear);
	
	public boolean saveCategory(CategoryModel categoryModel);
	public boolean saveArea(AreaModel areaModel);
	public boolean saveYear(YearModel yearModel);
	
	public boolean deleteCategory(Long uid);
	public boolean deleteArea(Long uid);
	public boolean deleteYear(Long uid);
	
	public List<AreaModel> getAllArea();
	public List<CategoryModel> getAllCategory();
	public List<YearModel> getAllYear();
	public List<YearModel> getAllYearOrderBy(String param);
	
	public AreaModel getAreaById(Long id);
	public CategoryModel getCategoryById(Long id);
	public YearModel getYearById(Long id);
	
	public MapImageModel getImageByMapInfoId(Long id);
}
