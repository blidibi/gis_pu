package com.pu.gis.dao.db.base;

import java.util.List;

import org.springframework.stereotype.Component;

import com.pu.gis.dao.db.generic.GenerikDao;
import com.pu.gis.model.MapInfoModel;

@Component
public class MapInfoDao extends GenerikDao<MapInfoModel>{

	@SuppressWarnings("unchecked")
	public List<MapInfoModel> getMapFilter(Long idArea, Long idCategory, Long idYears) {
        return sessionFactory.getCurrentSession().createQuery("FROM MapInfoModel M WHERE M.area.id = :area  and M.category.id = :categori and M.year.id = :year")
        		.setParameter("area", idArea)
        		.setParameter("categori", idCategory)
        		.setParameter("year", idYears)
        		.list();
    }
}
