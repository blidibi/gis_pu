package com.pu.gis.dao.db.base;

import org.springframework.stereotype.Component;
import com.pu.gis.dao.db.generic.GenerikDao;
import com.pu.gis.model.MapImageModel;

@Component
public class MapImageDao extends GenerikDao<MapImageModel>{
	
	public MapImageModel getMapImageModel(Long id) {
        return (MapImageModel) sessionFactory.getCurrentSession().createQuery("FROM MapImageModel M WHERE M.mapInfo.id = :id ")
        		.setParameter("id", id)
        		.uniqueResult();
    }
}
