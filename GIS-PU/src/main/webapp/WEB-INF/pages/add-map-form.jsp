<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Add map page</title>
</head>
<body>
	<h1>Add map page</h1>

	<form:form method="POST" modelAttribute="uploadedFile"
		action="${pageContext.request.contextPath}/map/add/process.html"
		enctype="multipart/form-data">
		<table>
			<tbody>
				<tr>
					<td>Title:</td>
					<td><input type="text" name="title" /></td>
				</tr>
				<tr>
					<td>Description:</td>
					<td><input type="text" name="description" /></td>
				</tr>
				<tr>
					<td>Longitude:</td>
					<td><input type="text" name="longitude" /></td>
				</tr>
				<tr>
					<td>Latitude:</td>
					<td><input type="text" name="latitude" /></td>
				</tr>
				<tr>
					<td>Image:</td>
					<td><input name="files[0]" type="file" /><br />
					<input name="files[1]" type="file" /></td>
				</tr>

				<tr>
					<td><input type="submit" value="Add" /></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</form:form>
	<p>
		<a href="${pageContext.request.contextPath}/index.html">Home page</a>
	</p>
</body>
</html>