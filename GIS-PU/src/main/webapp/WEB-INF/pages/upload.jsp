<!DOCTYPE html>
<html lang="en">
<head>
<title>Uploadify test</title>
<script
	src="${pageContext.request.contextPath}/resources/uploadify/jquery-1.3.2.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/uploadify/swfobject.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/uploadify/jquery.uploadify.v2.1.0.min.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/home.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function() {
		$('#uploadify').uploadify({
			'uploader' : 'uploadify/uploadify.swf',
			'script' : '/map/add/process',
			'folder' : '/uploads',
			'cancelImg' : 'uploadify/cancel.png'
		});
		$('#upload').click(function() {
			$('#uploadify').uploadifyUpload();
			return false;
		});
	});
</script>
</head>
<body>
	<h1>Hello World</h1>
	<input id="uploadify" type="file">
	<a id="upload" href="#">Upload</a>
</body>
</html>