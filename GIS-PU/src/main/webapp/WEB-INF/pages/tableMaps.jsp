<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<link
	href="${pageContext.request.contextPath}/resources/css/metro-bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/metro-bootstrap-responsive.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/iconFont.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/docs.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/js/prettify/prettify.css"
	rel="stylesheet">

<!-- Load JavaScript Libraries -->
<script
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.widget.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.mousewheel.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/prettify/prettify.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/holder/holder.js"></script>

<!-- Metro UI CSS JavaScript plugins -->
<script
	src="${pageContext.request.contextPath}/resources/js/metro.min.js"></script>

<!-- Local JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/docs.js"></script>

<!--wysisyg-->
<script
	src="${pageContext.request.contextPath}/resources/wysiwyg-plugin/tinymce.min.js"></script>

<title>Dinas PU - GIS</title>
<style>
#paneltombol {
	position: absolute;
	top: 5px;
	left: 50%;
	margin-left: -180px;
	z-index: 5;
	background-color: #fff;
	padding: 5px;
	border: 1px solid #999;
}
</style>
</head>
<body class="metro">
	<header class="bg-dark">
	<div class="navigation-bar fixed-top">
		<div class="navigation-bar-content container">
			<a href="/" class="element"><span class="icon-grid-view"></span>
				Dinas PU - GIS<sup>1.0</sup></a> <span class="element-divider"></span> <a
				class="element1 pull-menu" href="#"></a>
			<ul class="element-menu">
				<li><a class="dropdown-toggle" href="#">Components</a>
					<ul class="dropdown-menu dark" data-role="dropdown">
						<li><a href="tiles.html">Tiles</a></li>
						<li><a href="#" class="dropdown-toggle">Navigation</a>
							<ul class="dropdown-menu dark" data-role="dropdown">
								<li><a href="navbar.html">Navigation Bar</a></li>
								<li><a href="menus.html">Menus</a></li>
								<li><a href="fluent-menu.html">Fluent Menu</a></li>
								<li><a href="sidebar.html">Sidebar</a></li>
								<li><a href="tab-control.html">Tab Control</a></li>
								<li><a href="accordion.html">Accordion</a></li>
								<li><a href="buttons.html#_set">Button Set</a></li>
								<li><a href="buttons.html#_breadcrumbs">Breadcrumbs</a></li>
								<li><a href="wizard.html">Wizard</a></li>
							</ul></li>
						<li><a href="#" class="dropdown-toggle">Visualisation</a>
							<ul class="dropdown-menu dark" data-role="dropdown">
								<li><a href="rating.html">Rating</a></li>
								<li><a href="progress-bar.html">Progress Bar</a></li>
								<li><a href="scroll.html">Scroll Bar</a></li>
								<li><a href="slider.html">Slider</a></li>
								<li><a href="carousel.html">Carousel</a></li>
								<li><a href="treeview.html">TreeView</a></li>
								<li><a href="lists.html">Lists</a></li>
								<li><a href="hint.html">Hint</a></li>
								<li><a href="balloon.html">Balloon</a></li>
								<li><a href="notices.html">Notices</a></li>
								<li><a href="stepper.html">Stepper</a></li>
								<li><a href="panels.html">Panel</a></li>
								<li><a href="streamer.html">Streamer</a></li>
							</ul></li>
						<li><a href="#" class="dropdown-toggle">Date and Time</a>
							<ul class="dropdown-menu dark" data-role="dropdown">
								<li><a href="calendar.html">Calendar</a></li>
								<li><a href="datepicker.html">DatePicker</a></li>
								<li><a href="times.html">Times</a></li>
							</ul></li>
						<li><a href="#" class="dropdown-toggle">Information</a>
							<ul class="dropdown-menu dark" data-role="dropdown">
								<li><a href="window.html">Window</a></li>
								<li><a href="dialog.html">Dialog</a></li>
								<li><a href="notify.html">Notify</a></li>
							</ul></li>
						<li class="divider"></li>
						<li><a href="#" class="dropdown-toggle">Third-party</a>
							<ul class="dropdown-menu dark" data-role="dropdown">
								<li><a href="dataTables.html">DataTables</a></li>
							</ul></li>
					</ul>
		</div>
	</div>
	</header>
	<div class="container">
		<h3 id="_hovered">
		<i class=" icon-arrow-right-2 fg-darker smaller"></i>
				List Data
			<small class="on-right"></small>
		</h3>
		<p class="description">Keterangan List Data</p>

		<div class="example">
			<table class="table hovered">
				<thead>
					<tr>
						<th class="text-left">Title</th>
						<th class="text-left">Date Occurrence</th>
						<th class="text-left">Action</th>
					</tr>
				</thead>
				<tbody>
					<div id="tabelnya">
						<c:forEach var="mapinfo" items="${maps}">
							<tr id="tr_${mapinfo.uid}">
								<td id="mapinfo_${mapinfo.uid}">${mapinfo.title}</td>
								<td>${mapinfo.date}</td>
								<td class="right">
									<button class="button warning">Edit</button>
									<button class="button danger">Delete</button>
								</td>
							</tr>
						</c:forEach>
					</div>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End of container -->
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		var helper="";
		$('#tombolSubmit').click(function() {
			var stat = $('#status').val();
			var urlPost = "";
			var idnya=$("#uid").val();
			console.log($('#status').val());
			if (stat == "save")
				urlPost = "${pageContext.request.contextPath}/area/add";
			else
				urlPost = "${pageContext.request.contextPath}/area/edit";
			$.ajax({
				url : urlPost,
				type : "POST",
				data : {
					"uid_area" : idnya,
					"area" : $("#area").val()
				},
				success : function(status) {
					console.log(status);
					if (status == 'OK') {
						alert("Succes Update Data");
						//if (stat != "save") $("#area_"+idnya).html(helper);
						//else 
						location.reload();
						$("#uid").val("");
			            $("#area").val("");
			            $("#status").val("save");
						$("#changemod").hide();
					} else {
						alert("Failed Update Data");
					}
				},
				error : function(e) {
					alert("ERROR"+e);
				}
			});
		});
		
		$("#changemod").hide();
		
    	$(".warning").click(function() {
            var id = $(this).parent().parent().attr('id').split("tr_")[1];
            $("#uid").val(id);
            helper=$("#area_"+id).text();
            $("#area").val($("#area_"+id).text());
            $("#status").val("edit");
            $("#changemod").show();
        });
    	$("#changemod").click(function() {
            $("#uid").val("");
            $("#area").val("");
            $("#status").val("save");
            $("#changemod").hide();
        });
		$(".danger").click(function() {
			var id_tr=$(this).parent().parent().attr('id').split("tr_")[1];
			$.ajax({
				url : "${pageContext.request.contextPath}/area/delete",
				type : "POST",
				data : {
					"uid_area" : id_tr
				},
				success : function(status) {
					console.log(status);
					if (status == 'OK') {
						alert("Succes Delete Data");
						$("#tr_"+id_tr).remove();
					} else {
						console.log(status);
						alert("Failed Delete Data");
					}
				},
				error : function(e) {
					console.log(e);
					alert("ERROR"+e);
				}
			});
			
		});
	});
</script>