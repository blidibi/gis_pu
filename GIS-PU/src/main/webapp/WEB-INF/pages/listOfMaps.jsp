<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">

<link
	href="${pageContext.request.contextPath}/resources/css/metro-bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/metro-bootstrap-responsive.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/iconFont.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/docs.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/js/prettify/prettify.css"
	rel="stylesheet">

<!-- Load JavaScript Libraries -->
<script
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.widget.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.mousewheel.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/prettify/prettify.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/holder/holder.js"></script>

<!-- Metro UI CSS JavaScript plugins -->
<script
	src="${pageContext.request.contextPath}/resources/js/metro.min.js"></script>

<!-- Local JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/docs.js"></script>

<!--wysisyg-->
<script
	src="${pageContext.request.contextPath}/resources/wysiwyg-plugin/tinymce.min.js"></script>

<title>Dinas PU - GIS</title>
<style>
#paneltombol {
	position: absolute;
	top: 5px;
	left: 50%;
	margin-left: -180px;
	z-index: 5;
	background-color: #fff;
	padding: 5px;
	border: 1px solid #999;
}
</style>
</head>
<body class="metro">
	<header class="bg-dark">
		<div class="navigation-bar fixed-top">
			<div class="navigation-bar-content container">
				<a href="/" class="element"><span class="icon-grid-view"></span>
					Dinas PU - GIS<sup>1.0</sup></a> <span class="element-divider"></span>
				<a class="element1 pull-menu" href="#"></a>
				<ul class="element-menu">
					<li><a class="dropdown-toggle" href="#">Components</a>
						<ul class="dropdown-menu dark" data-role="dropdown">
							<li><a href="tiles.html">Tiles</a></li>
							<li><a href="#" class="dropdown-toggle">Navigation</a>
								<ul class="dropdown-menu dark" data-role="dropdown">
									<li><a href="navbar.html">Navigation Bar</a></li>
									<li><a href="menus.html">Menus</a></li>
									<li><a href="fluent-menu.html">Fluent Menu</a></li>
									<li><a href="sidebar.html">Sidebar</a></li>
									<li><a href="tab-control.html">Tab Control</a></li>
									<li><a href="accordion.html">Accordion</a></li>
									<li><a href="buttons.html#_set">Button Set</a></li>
									<li><a href="buttons.html#_breadcrumbs">Breadcrumbs</a></li>
									<li><a href="wizard.html">Wizard</a></li>
								</ul></li>
							<li><a href="#" class="dropdown-toggle">Visualisation</a>
								<ul class="dropdown-menu dark" data-role="dropdown">
									<li><a href="rating.html">Rating</a></li>
									<li><a href="progress-bar.html">Progress Bar</a></li>
									<li><a href="scroll.html">Scroll Bar</a></li>
									<li><a href="slider.html">Slider</a></li>
									<li><a href="carousel.html">Carousel</a></li>
									<li><a href="treeview.html">TreeView</a></li>
									<li><a href="lists.html">Lists</a></li>
									<li><a href="hint.html">Hint</a></li>
									<li><a href="balloon.html">Balloon</a></li>
									<li><a href="notices.html">Notices</a></li>
									<li><a href="stepper.html">Stepper</a></li>
									<li><a href="panels.html">Panel</a></li>
									<li><a href="streamer.html">Streamer</a></li>
								</ul></li>
							<li><a href="#" class="dropdown-toggle">Date and Time</a>
								<ul class="dropdown-menu dark" data-role="dropdown">
									<li><a href="calendar.html">Calendar</a></li>
									<li><a href="datepicker.html">DatePicker</a></li>
									<li><a href="times.html">Times</a></li>
								</ul></li>
							<li><a href="#" class="dropdown-toggle">Information</a>
								<ul class="dropdown-menu dark" data-role="dropdown">
									<li><a href="window.html">Window</a></li>
									<li><a href="dialog.html">Dialog</a></li>
									<li><a href="notify.html">Notify</a></li>
								</ul></li>
							<li class="divider"></li>
							<li><a href="#" class="dropdown-toggle">Third-party</a>
								<ul class="dropdown-menu dark" data-role="dropdown">
									<li><a href="dataTables.html">DataTables</a></li>
								</ul></li>
						</ul>
			</div>
		</div>
	</header>
	<div class="container">
		<h2>
			<a href="/"><i class=" icon-arrow-right-2 fg-darker smaller"></i></a>
			Map Data<small class="on-right"></small>
		</h2>

		<div class="example after">
			<form method="POST" action="${pageContext.request.contextPath}/map/search.html">
				<fieldset>
					<label>Category</label>
					<div class="input-control select size5">
						<select name="category" id="category">
							<option value="0">--Select Category--</option>
							<c:forEach var="list" items="${listCategory}">
								<option value="${list.id}">${list.category}</option>
							</c:forEach>
						</select>
					</div>
					<label>Area</label>
					<div class="input-control select size5">
						<select name="area" id="area">
							<option value="0">--Select Area--</option>
							<c:forEach var="list" items="${listArea}">
								<option value="${list.id}">${list.area}</option>
							</c:forEach>
						</select>
					</div>
					<label>Year</label>
					<div class="input-control select size2">
						<select name="year" id="year">
							<option value="0">--Select Year--</option>
							<c:forEach var="list" items="${listYear}">
								<option value="${list.id}">${list.year}</option>
							</c:forEach>
						</select>
					</div>
					<div style="margin-top: 20px"></div>
					<div class="input-control" data-role="input-control">
						<input type="submit" value="Search"> <input type="reset"
							value="Reset">
					</div>
					<div style="margin-top: 20px"></div>
				</fieldset>
			</form>
		</div>
	</div>
	<!-- End of container -->
</body>
</html>