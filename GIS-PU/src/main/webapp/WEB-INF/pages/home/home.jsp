<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

<link href="${pageContext.request.contextPath}/resources/css/metro-bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/metro-bootstrap-responsive.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/iconFont.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/docs.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/js/prettify/prettify.css" rel="stylesheet">

		<!-- Load JavaScript Libraries -->
<script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.widget.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.mousewheel.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/prettify/prettify.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/holder/holder.js"></script>

<!-- Metro UI CSS JavaScript plugins -->
<script src="${pageContext.request.contextPath}/resources/js/metro.min.js"></script>
		<!-- Local JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/docs.js"></script>
<title>Dinas PU - GIS 1.0</title>
<style type="text/css">
.container-fluid {
  margin: 0 auto;
  height: 100%;
  padding: 20px 0;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

.navigation-bar {
  margin: 0 auto;
  height: 100%;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

#paneltombol {
  position: absolute;
  top: 50px;
  left: 50%;
  margin-left: -300px;
  z-index: 5;
  background-color: #fff;
  padding: 5px;
  border: 1px solid #999;
}
</style>
</head>
<body class="metro">
  <header class="bg-dark">
  <div class="navigation-bar dark fixed-top">
    <div class="navigation-bar-content container">
      <a href="/" class="element"><span class="icon-grid-view"></span>
        Dinas PU - GIS<sup>1.0</sup></a> <span class="element-divider"></span> <a
        class="element1 pull-menu" href="#"></a>
      <ul class="element-menu">
        <li><a class="dropdown-toggle" href="#">Components</a>
          <ul class="dropdown-menu dark" data-role="dropdown">
            <li><a href="tiles.html">Tiles</a></li>
            <li><a href="#" class="dropdown-toggle">Navigation</a>
              <ul class="dropdown-menu dark" data-role="dropdown">
                <li><a href="navbar.html">Navigation Bar</a></li>
                <li><a href="menus.html">Menus</a></li>
                <li><a href="fluent-menu.html">Fluent Menu</a></li>
                <li><a href="sidebar.html">Sidebar</a></li>
                <li><a href="tab-control.html">Tab Control</a></li>
                <li><a href="accordion.html">Accordion</a></li>
                <li><a href="buttons.html#_set">Button Set</a></li>
                <li><a href="buttons.html#_breadcrumbs">Breadcrumbs</a></li>
                <li><a href="wizard.html">Wizard</a></li>
              </ul></li>
            <li><a href="#" class="dropdown-toggle">Visualisation</a>
              <ul class="dropdown-menu dark" data-role="dropdown">
                <li><a href="rating.html">Rating</a></li>
                <li><a href="progress-bar.html">Progress Bar</a></li>
                <li><a href="scroll.html">Scroll Bar</a></li>
                <li><a href="slider.html">Slider</a></li>
                <li><a href="carousel.html">Carousel</a></li>
                <li><a href="treeview.html">TreeView</a></li>
                <li><a href="lists.html">Lists</a></li>
                <li><a href="hint.html">Hint</a></li>
                <li><a href="balloon.html">Balloon</a></li>
                <li><a href="notices.html">Notices</a></li>
                <li><a href="stepper.html">Stepper</a></li>
                <li><a href="panels.html">Panel</a></li>
                <li><a href="streamer.html">Streamer</a></li>
              </ul></li>
            <li><a href="#" class="dropdown-toggle">Date and Time</a>
              <ul class="dropdown-menu dark" data-role="dropdown">
                <li><a href="calendar.html">Calendar</a></li>
                <li><a href="datepicker.html">DatePicker</a></li>
                <li><a href="times.html">Times</a></li>
              </ul></li>
            <li><a href="#" class="dropdown-toggle">Information</a>
              <ul class="dropdown-menu dark" data-role="dropdown">
                <li><a href="window.html">Window</a></li>
                <li><a href="dialog.html">Dialog</a></li>
                <li><a href="notify.html">Notify</a></li>
              </ul></li>
            <li class="divider"></li>
            <li><a href="#" class="dropdown-toggle">Third-party</a>
              <ul class="dropdown-menu dark" data-role="dropdown">
                <li><a href="dataTables.html">DataTables</a></li>
              </ul></li>
          </ul>
    </div>
  </header>
  <div id="paneltombol">
      Category:
      <select name="category" id="category">
        <option value="0">--Select Category--</option>
        <c:forEach var="list" items="${listCategory}">
          <option value="${list.id}">${list.category}</option>
        </c:forEach>
      </select>
      Area:
      <select name="area" id="area">
        <option value="0">--Select Area--</option>
        <c:forEach var="list" items="${listArea}">
          <option value="${list.id}">${list.area}</option>
        </c:forEach>
      </select>
      Year:
      <select name="year" id="year">
        <option value="0">--Select Year--</option>
        <c:forEach var="list" items="${listYear}">
          <option value="${list.id}">${list.year}</option>
        </c:forEach>
      </select>
    <input type=button value="submit" id="tomSubmit">
  </div>
  <div class="container-fluid" id="googleMap"></div>
  </div>
  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDoSNDCskUG0BBafjH10-KueWY3Eld69-U&sensor=false"></script>
  <script>
    var map;
  var myCenter = new google.maps.LatLng(-7.782935, 110.367007);
  var markers = [];
  function initialize() {

    var mapProp = {
      center : myCenter,
      zoom : 12,
      mapTypeId : google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    google.maps.event.addListener(map, 'click', function(event) {
      addMarker(event.latLng);
    });
  }
  function addMarker(location) {
    deleteMarkers();
    var marker = new google.maps.Marker({
      position : location,
      map : map
    });
    $("#latvalue").val(location.lat());
    $("#longvalue").val(location.lng());
    markers.push(marker);
  }
  function clearMarkers() {
    setAllMap(null);
  }
  function setAllMap(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }
  function deleteMarkers() {
    clearMarkers();
    markers = [];
  }
  function showMarkers() {
    setAllMap(map);
  }

  google.maps.event.addDomListener(window, 'load', initialize);
    function resizebro() {
      map.setCenter(myCenter);
    }
    google.maps.event.addDomListener(window, 'resize', resizebro);
    var center;
    function calculateCenter() {
      center = map.getCenter();
    }
    google.maps.event.addDomListener(map, 'idle', calculateCenter);
  </script>
<script type="text/javascript">
  function addMarkerFromServer(lang,lot,title,tanggal,uid) {
    var marker = new google.maps.Marker({
      position : new google.maps.LatLng(lang, lot),
      map : map
    });
    markers.push(marker);
    // var contentString = "<b>title</b>: "+title+"<br><b>date</b>: "+tanggal+"<br>coordinate: "+lang+","+lot+"<br>";
    var contentString = 
      '<div>'+
      '<br><b>Title</b> :'+title+
      '<br><b>Date</b> :'+tanggal+
      '<br><b>Coordinate</b> :'+lang+','+lot+
      '<br><a href="${pageContext.request.contextPath}/gis/detail_location/'+uid+'"><b>Detail</b></a>'
      '</div>';
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });
  }
  $(document).ready(function() {
    var helper="";
    $('#tomSubmit').click(function() {
      var urlPost = "${pageContext.request.contextPath}/gis/getdata";
      $.ajax({
        url : urlPost,
        type : "POST",
        data : {
          "area" : $("#area").val(),
          "year" : $("#year").val(),
          "category" : $("#category").val()
        },
        success : function(status) {
          console.log(status);
          if(status=="[]") alert("No Data")
          else{
          	var myArr = $.parseJSON(status);
		      for(var k in myArr) {
		         addMarkerFromServer(myArr[k]['latitude'],myArr[k]['longitude'],myArr[k]['title'],myArr[k]['date'],myArr[k]['id']);
		      }
		      showMarkers();
          }
        },
        error : function(e) {
          alert("ERROR GET DATA"+e);
        }
      });
    });
  });
</script>
</body>
</html>