<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

<link href="${pageContext.request.contextPath}/resources/css/metro-bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/metro-bootstrap-responsive.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/iconFont.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/docs.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/js/prettify/prettify.css" rel="stylesheet">

		<!-- Load JavaScript Libraries -->
<script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.widget.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.mousewheel.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/prettify/prettify.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/holder/holder.js"></script>

<!-- Metro UI CSS JavaScript plugins -->
<script src="${pageContext.request.contextPath}/resources/js/metro.min.js"></script>
		<!-- Local JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/docs.js"></script>
<title>Dinas PU - GIS 1.0</title>
</head>
<body class="metro">
    <header class="bg-dark">
    <div class="navigation-bar fixed-top">
        <div class="navigation-bar-content container">
            <a href="/" class="element"><span class="icon-grid-view"></span> Dinas PU - GIS<sup>1.0</sup></a>
            <span class="element-divider"></span>
            <a class="element1 pull-menu" href="#"></a>
            <ul class="element-menu">
            <li>
                <a class="dropdown-toggle"  href="#">Components</a>
                <ul class="dropdown-menu dark" data-role="dropdown">
                    <li><a href="tiles.html">Tiles</a></li>
                    <li>
                        <a href="#" class="dropdown-toggle">Navigation</a>
                        <ul class="dropdown-menu dark" data-role="dropdown">
                            <li><a href="navbar.html">Navigation Bar</a></li>
                            <li><a href="menus.html">Menus</a></li>
                            <li><a href="fluent-menu.html">Fluent Menu</a></li>
                            <li><a href="sidebar.html">Sidebar</a></li>
                            s<li><a href="tab-control.html">Tab Control</a></li>
                            <li><a href="accordion.html">Accordion</a></li>
                            <li><a href="buttons.html#_set">Button Set</a></li>
                            <li><a href="buttons.html#_breadcrumbs">Breadcrumbs</a></li>
                            <li><a href="wizard.html">Wizard</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle">Visualisation</a>
                        <ul class="dropdown-menu dark" data-role="dropdown">
                            <li><a href="rating.html">Rating</a></li>
                            <li><a href="progress-bar.html">Progress Bar</a></li>
                            <li><a href="scroll.html">Scroll Bar</a></li>
                            <li><a href="slider.html">Slider</a></li>
                            <li><a href="carousel.html">Carousel</a></li>
                            <li><a href="treeview.html">TreeView</a></li>
                            <li><a href="lists.html">Lists</a></li>
                            <li><a href="hint.html">Hint</a></li>
                            <li><a href="balloon.html">Balloon</a></li>
                            <li><a href="notices.html">Notices</a></li>
                            <li><a href="stepper.html">Stepper</a></li>
                            <li><a href="panels.html">Panel</a></li>
                            <li><a href="streamer.html">Streamer</a></li>
                        </ul>
                    </li>
                <li>
                    <a href="#" class="dropdown-toggle">Date and Time</a>
                    <ul class="dropdown-menu dark" data-role="dropdown">
                        <li><a href="calendar.html">Calendar</a></li>
                        <li><a href="datepicker.html">DatePicker</a></li>
                        <li><a href="times.html">Times</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle">Information</a>
                    <ul class="dropdown-menu dark" data-role="dropdown">
                        <li><a href="window.html">Window</a></li>
                        <li><a href="dialog.html">Dialog</a></li>
                        <li><a href="notify.html">Notify</a></li>
                    </ul>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#" class="dropdown-toggle">Third-party</a>
                    <ul class="dropdown-menu dark" data-role="dropdown">
                        <li><a href="dataTables.html">DataTables</a></li>
                    </ul>
                </li>
            </ul>
        </div>  
    </div>
    </header>
    <div class="container">
        <h2>
            <a href="/"><i class=" icon-location fg-darker smaller"></i></a>
            ${mapModel.title}<small class="on-right"></small>
        </h2>
        <div class="example after">
            <div class="grid">
                <div class="row">
                    <div class="span6 jumbotron" id="googleMap" style="height:300px;"></div>
                    <div class="span6">
                        <p class="tertiary-text">
                            <h3>Kategory</h3>
                            ${mapModel.category.category}
                            <h3>Lokasi</h3>
                            ${mapModel.area.area}
                            <h3>Tanggal</h3>
                            ${mapModel.date}
                            <h3>Koordinat</h3>
                            ${mapModel.latitude},${mapModel.longitude}

                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="span12">
                        <p class="tertiary-text">
                            <blockquote>
                                <h2>Description</h2>
                            </blockquote>
                            ${mapModel.description}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="span12">
                        <p>
                            <blockquote>
                                <p><h2>Photo</h2></p>
                            </blockquote>
                            <div class="image-container shadow span6">
								<img src="<%=request.getContextPath()%>/SendImage/<%=request.getRealPath("/")%>/${img.imageName}">
									<div class="overlay-fluid">${mapModel.title}</div>
							</div>
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
    </div> <!-- End of container -->


</body>
</html>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDoSNDCskUG0BBafjH10-KueWY3Eld69-U&sensor=false"></script>
<script>
$(".dropdown-menu li a").click(function(){
  var selText = $(this).text();
  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});  

var myCenter=new google.maps.LatLng(${mapModel.latitude},${mapModel.longitude});
function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:12,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  animation: google.maps.Animation.DROP,
  });
marker.setMap(map);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>