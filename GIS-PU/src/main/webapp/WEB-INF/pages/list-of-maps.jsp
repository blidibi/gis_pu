<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>List of maps</title>
</head>
<body>
	<h1>List of maps</h1>
	<p>Here you can see the list of the maps, edit them, remove or
		update.</p>
	<table border="1px" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th width="15%">title</th>
				<th width="10%">longitude</th>
				<th width="10%">latitude</th>
				<th width="10%">images</th>
				<th width="10%">actions</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="map" items="${maps}">
				<tr>
					<td>${map.title}</td>
					<td>${map.longitude}</td>
					<td>${map.latitude}</td>
					<td><c:forEach var="img" items="${map.images}">
						<img src="${pageContext.request.contextPath}/resources/images/${img.imageName}" width="25%" height="25%"/>
					</c:forEach></td>
					<td><a
						href="${pageContext.request.contextPath}/map/edit/${map.uid}.html">Edit</a><br />
						<a
						href="${pageContext.request.contextPath}/map/delete/${map.uid}.html">Delete</a><br />
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<p>
		<a href="${pageContext.request.contextPath}/index.html">Home page</a>
	</p>
</body>
</html>